from framework.infrastructure.interfaces import Repository


class HackerNewsRepositoryBridge(Repository):

    def __init__(self, repository):
        self._repository = repository

    def save(self, story):
        return self._repository.save(story)

    def get(self, id):
        return self._repository.get(id)