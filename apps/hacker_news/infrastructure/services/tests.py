import unittest
from .service import HackerNewsService


class FeedRepositoryTestClass(unittest.TestCase):

    def setUp(self):
        self.service = HackerNewsService()

    def test_get_stories(self):
        stories = self.service.get_stories()
        self.assertCountEqual(len(stories), 10)


# runs the unit tests in the module
if __name__ == '__main__':
    unittest.main()