class Comment:
    def __init__(self, comment_id: str = None, author: str = None, text: str = None, date_created: str = None) -> None:
            self.comment_id = comment_id
            self.author = author
            self.text = text
            self.date_created = date_created
