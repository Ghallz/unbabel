from typing import NamedTuple
from framework.usecases import InputPort
from ..domain import Story, Comment, StoriesNotFound


class GetStoriesResponse(NamedTuple):
    success: bool
    message: str
    data: tuple


class GetStoriesUseCase(InputPort):

    def __call__(self) -> GetStoriesResponse:
        try:
            stories = self._service.get_stories()
            response = GetStoriesResponse(
                success=True,
                message='',
                data=stories
                # data=TodoListData(**todolist.__dict__)
            )
        except StoriesNotFound:
            response = GetStoriesResponse(
                success=False,
                message='Stories Not Fount.',
                data=()
            )
        return self._presenter(response)