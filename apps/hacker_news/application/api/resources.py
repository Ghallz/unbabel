from flask_restful import Resource
from ...usecases import GetStoriesUseCase
from ...application import presenters
from .adapters import HackerNewsApiAdapter
from ...infrastructure import HackerNewsService, HackerNewsRepositoryBridge, HackerNewsRepository
from extensions import db, story_collection


class HackerNews(Resource):

    def get(self):

        usecase = GetStoriesUseCase(
            service=HackerNewsService(),
            repository= HackerNewsRepositoryBridge(HackerNewsRepository, db, story_collection),
            presenter=presenters.GetStoriesPresenter(adapter=HackerNewsApiAdapter())
        )

        return usecase()