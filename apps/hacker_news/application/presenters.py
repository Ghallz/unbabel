from framework.usecases import OutputPort

from ..usecases import GetStoriesResponse


class GetStoriesPresenter(OutputPort):

    def __call__(self, response: GetStoriesResponse) -> GetStoriesResponse:
        return self._adapter(response)