from uuid import uuid4
from todo.domain import TodoList
from todo.domain import Todo
from todo.domain import TodoListNotFoundError
from framework.infrastructure.interfaces import Repository


class TodoListMockedRepository(Repository):

    def __init__(self, session):
        self._store = [TodoList("d0f19d156e154946b74a8ed150d26e5d", "1st Todo List")]
        self.session = session

    def save(self, todolist):
        if not todolist.id:
            todolist.id = "d0f19d156e154946b74a8ed150d26e5d" # Hard coded id for test porpouse
        if not todolist in self._store:
            self._store.append(todolist)
            self._save_todos(todolist)
        return todolist

    def get(self, id):
        try:
            return next(filter(lambda x: x.id == id,  self._store))
        except StopIteration:
            raise TodoListNotFoundError

    def delete(self, id):
        todolist = self.get(id)
        return self._store.remove(todolist)
    
    def filter(self, filters):
        def map_filter(obj):
            for item in filters.items():
                try:
                    if not obj.__getattribute__(item[0]) == item[1]:
                        return False
                except AttributeError:
                    return False
            else:
                return True

        return list(filter(lambda obj: map_filter(obj),  self._store))

    def _save_todos(self, todolist):
        for todo in todolist:
            if not todo.id:
                todo.id = uuid4().hex