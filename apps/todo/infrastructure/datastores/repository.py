from framework.infrastructure.interfaces import Repository


class TodoListRepository(Repository):

    def __init__(self, concrete_repository, session):
        self._repository = concrete_repository(session)

    def save(self, todolist):
        return self._repository.save(todolist)

    def get(self, id):
       return self._repository.get(id)

    def delete(self, id):
        return self._repository.delete(id)
    
    def filter(self, filters):
        return self._repository.filter(filters)