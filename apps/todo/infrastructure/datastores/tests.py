from uuid import uuid4
from unittest import TestCase
from todo.domain import TodoList, Todo, TodoListNotFoundError
from todo.infrastructure import TodoListRepository, TodoListMockedRepository


class TodoListRepositoryTestCase(TestCase):

    def setUp(self):
        session = {}
        self.repository = TodoListRepository(TodoListMockedRepository, session)
        self.first_todo_list_id = uuid4().hex
        self.first_todolist = TodoList(self.first_todo_list_id, "1st Todo List")
        self.first_todo_id = uuid4().hex
        self.first_todo = Todo(self.first_todo_id, 'Do something')
        self.first_todolist.add(self.first_todo)
        self.repository.save(self.first_todolist)

    def test_create_todolist(self):
        todolist = TodoList(None, "2nd Todo List")
        created_todolist = self.repository.save(todolist)
        self.assertIsNotNone(todolist.id)

    def test_save(self):
        todolist = TodoList(2, "2nd Todo List")
        saved_todolist = self.repository.save(todolist)
        self.assertEqual(todolist, saved_todolist)

    def test_do_not_duplicate_todolist(self):
        id = uuid4().hex
        todolist = TodoList(id, "2nd Todo List")
        self.repository.save(todolist)
        self.repository.save(todolist)
        self.assertEqual(len(self.repository.filter({'id': id})), 1)

    def test_get(self):
        todolist = self.repository.get(self.first_todo_list_id)
        self.assertEqual(self.first_todolist, todolist)

    def test_get_not_in_repository(self):
        with self.assertRaises(TodoListNotFoundError):
            self.repository.get(uuid4().hex)
    
    def test_delete(self):
        self.repository.delete(self.first_todo_list_id)
        with self.assertRaises(TodoListNotFoundError):
            self.repository.get(self.first_todo_list_id)

    def test_delete_todolist_not_in_repository(self):
        with self.assertRaises(TodoListNotFoundError):
            self.repository.get(uuid4().hex)

    #### Test Todo handling by the repository ####

    def test_create_todo_within_todolist(self):
        todolist = TodoList(None, "2nd Todo List")
        todo = Todo(None, 'Do something')
        todolist.add(todo)
        created_todolist = self.repository.save(todolist)
        self.assertIsNotNone(todolist.todos[0].id)


    def test_save_todo_within_todolist(self):
        todolist = TodoList(uuid4().hex, "2nd Todo List")
        todo = Todo(uuid4().hex, 'Do something')
        todolist.add(todo)
        saved_todolist = self.repository.save(todolist)
        self.assertIn(todo, saved_todolist.todos)

    def test_delete_todo_within_todolist(self):
        todolist = self.repository.get(self.first_todo_list_id)
        todolist.remove(self.first_todo.id)
        self.repository.save(todolist)
        todolist_after_save = self.repository.get(self.first_todo_list_id)
        self.assertNotIn(self.first_todo, todolist_after_save.todos)

    def test_update_todo_within_todolist(self):
        todolist = self.repository.get(self.first_todo_list_id)
        todo = todolist[self.first_todo_id]
        new_description = 'Do something else'
        todo.description = new_description
        self.repository.save(todolist)
        todolist_after_save = self.repository.get(self.first_todo_list_id)
        todo_after_update = todolist_after_save.get(self.first_todo_id)
        self.assertEqual(todo_after_update.description, new_description)

    def tearDown(self):
        self.repository = None
        self.first_todolist = None
