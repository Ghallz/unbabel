from flask import Blueprint
from flask_restful import Api

from todo.application.api.resources import TodoList


todo_api_blueprint = Blueprint('todo', __name__)
api = Api(todo_api_blueprint)

api.add_resource(TodoList, '/todolists/', '/todolists/<string:id>')
