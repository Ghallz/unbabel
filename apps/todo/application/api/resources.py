from flask import request
from flask_restful import Resource
from marshmallow.exceptions import ValidationError

from framework.domain import EntityId
from todo import usecases
from todo.application import presenters
from todo.application.api.adapters import TodoListRestApiAdapter
from todo.application.api.serializers import TodoListSchema
from todo.infrastructure import TodoListMockedRepository, TodoListRepository


class TodoList(Resource):

    def get(self, id=None):

        if id:
            usecase = usecases.GetTodoListUseCase(
                repository=TodoListRepository(TodoListMockedRepository, 'session'),
                presenter=presenters.TodoListPresenter(adapter=TodoListRestApiAdapter())
            )
            rq = usecases.GetTodoListRequest(todolist_id=id)
        else:
            usecase = usecases.FilterTodoListsUseCase(
                repository=TodoListRepository(TodoListMockedRepository, 'session'),
                presenter=presenters.TodoListPresenter(adapter=TodoListRestApiAdapter())
            )
            rq = usecases.FilterTodoListsRequest(
                filters = request.args
            )
        return usecase(rq)
    
    def post(self):
        schema = TodoListSchema(only=['name'])
        try:
            loaded_data = schema.load(request.json)
            usecase = usecases.CreateTodoListUseCase(
                repository=TodoListRepository(TodoListMockedRepository, 'session'),
                presenter= presenters.TodoListPresenter(adapter=TodoListRestApiAdapter())
            )
            request_model = usecases.CreateTodoListRequest(**loaded_data)
            return usecase(request_model)
        except ValidationError as error:
            return error.messages, 400
