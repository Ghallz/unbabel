from marshmallow import Schema, fields


class Story(Schema):
    story_id = fields.Int()
    author = fields.Str()
    title = fields.Str()
    url = fields.Str()
    date_created = fields.Int()
    comments = fields.Nested(Comment, many=True)

class Comment(Schema):
    comment_id = fields.Int()
    author = fields.Str()
    text = fields.Str()
    date_created = fields.Int()


class ErrorResponseSchema(Schema):
    error = fields.Str()