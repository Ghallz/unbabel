import json
from todo.application.api.serializers import TodoListSchema, ErrorResponseSchema


class TodoListRestApiAdapter:
    """
    This adapter must convert the usecase response to a REST API compatible
    format, either in success or failure cases.
    """
    def __call__(self, response):
        if response.success:
            schema = TodoListSchema()
            if isinstance(response.data, list):
                return schema.dump(response.data, many=True), 200
            else:
                return schema.dump(response.data), 200
        else:
            schema = ErrorResponseSchema()
            return schema.dump({'error': response.message}), 404