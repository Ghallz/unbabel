from framework.usecases import OutputPort

from todo.usecases import CreateTodoListResponse


class TodoListPresenter(OutputPort):

    def __call__(self, response: CreateTodoListResponse) -> CreateTodoListResponse:
        return self._adapter(response)