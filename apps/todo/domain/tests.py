from uuid import uuid4
from unittest import TestCase
from todo.domain import TodoList, Todo, TodoNotFoundError


class TodoListTestCase(TestCase):

    def test_add_todo(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        todo_list.add(todo)
        self.assertIn(todo, todo_list.todos)

    def test_add_todo_to_empty_todos(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        todo_list.add(todo, 5)
        self.assertEqual(todo_list.index(todo), 0)

    def test_add_todo_in_order(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        todo_list.add(todo)
        todo2 = Todo(id=uuid4().hex, description='Do other thing')
        todo_list.add(todo2, 0)
        self.assertEqual(todo_list.index(todo2), 0)


    def test_reorder_todos_after_add(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        todo_list.add(todo)
        todo2 = Todo(id=uuid4().hex, description='Do other thing')
        todo_list.add(todo2, 0)
        self.assertEqual(todo_list.index(todo2), 0)
        self.assertEqual(todo_list.index(todo), 1)

    def test_remove_todo(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo_id = id=uuid4().hex
        todo = Todo(id=todo_id, description='Do something')
        todo_list.add(todo)
        todo_list.remove(todo_id)
        self.assertNotIn(todo, todo_list.todos)
    
    def test_remove_todo_not_in_list(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        with self.assertRaises(TodoNotFoundError):
            todo_list.remove(todo)

    def test_order_of_todo_not_in_list(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        with self.assertRaises(TodoNotFoundError):
            todo_list.index(todo)

    def test_reorder_todos_after_remove(self):
        # setup
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        todo_list.add(todo)
        todo2_id = id=uuid4().hex
        todo2 = Todo(id=todo2_id, description='Do other thing')
        todo_list.add(todo2, 0)
        # test
        todo_list.remove(todo2_id)
        self.assertEqual(todo_list.index(todo), 0)

    def test_toggle_todo_from_list(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        id=uuid4().hex
        todo = Todo(id, description='Do something')
        todo_list.add(todo)
        todo_list.toggle(id)
        self.assertEqual(todo.completed, True)
    
    def test_allow_only_unique_todo_in_list(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        todo_list.add(todo)
        todo_list.add(todo)
        self.assertEqual(len(todo_list.todos), 1)

    def test_move_todo_to_position(self):
        todo_list = TodoList(id=uuid4().hex, name='Test List', todos=[])
        todo = Todo(id=uuid4().hex, description='Do something')
        todo_list.add(todo)
        todo2 = Todo(id=uuid4().hex, description='Do other thing')
        todo_list.add(todo2)
        todo_list.move_to(0, todo2)
        self.assertEqual(todo_list.index(todo2), 0)
        self.assertEqual(todo_list.index(todo), 1)