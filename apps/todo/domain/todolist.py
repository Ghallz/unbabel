from typing import List
from framework.domain import EntityId, Entity
from todo.domain.todo import Todo
from todo.domain.exceptions import TodoNotFoundError


class TodoList(Entity):

    def __init__(self, id: EntityId, name: str, todos: List[Todo] = None) -> None:
        self.id = id
        self.name = name
        self.todos = todos if todos else []

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return f'<TodoList: {self.id}>'

    def __getitem__(self, id: EntityId):
        return self.get(id)

    def __len__(self):
        return len(self.todos)

    def __iter__(self):
        return iter(self.todos)

    def add(self, todo: Todo, position: int = None) -> None:
        if not todo in self.todos:
            if position is None:
                self.todos.append(todo)
            else:
                self.todos.insert(position, todo)

    def remove(self, id: EntityId) -> None:
        try:
            todo = self.get(id)
            self.todos.remove(todo)
        except ValueError:
            raise TodoNotFoundError

    def index(self, todo: Todo):
        try:
            return self.todos.index(todo)
        except ValueError:
            raise TodoNotFoundError

    def get(self, id: EntityId):
        try:
            return next(filter(lambda x: x.id == id,  self.todos))
        except StopIteration:
            raise TodoNotFoundError

    def toggle(self, id: EntityId):
        internal_todo = self.get(id)
        internal_todo.toggle()
        return internal_todo

    def move_to(self, position: int, todo: Todo) -> None:
        current_position = self.index(todo)
        todo = self.todos.pop(current_position)
        self.add(todo, position)