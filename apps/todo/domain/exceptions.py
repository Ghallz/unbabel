class TodoListNotFoundError(LookupError):
    pass


class TodoNotFoundError(LookupError):
    pass