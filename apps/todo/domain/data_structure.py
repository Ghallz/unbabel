from typing import NamedTuple, List
from uuid import UUID


class TodoListData(NamedTuple):
    id: UUID
    name: str
    todos: List


class TodoData(NamedTuple):
    id: UUID
    description: str
    completed: bool
