from uuid import UUID
from framework.domain import Entity


class Todo(Entity):

    def __init__(self, id: UUID, description: str, completed: bool = False) -> None:
        self.id = id
        self.description = description
        self.completed = completed

    def __repr__(self):
        return f'<Todo: {self.id}>'

    def __eq__(self, other):
        return hash(self.id) == hash(other.id)

    def toggle(self):
        self.completed = not self.completed