from uuid import UUID
from typing import NamedTuple

from framework.usecases.interfaces import InputPort, OutputPort
from framework.infrastructure.interfaces import Repository

from todo.domain import TodoListNotFoundError


class DeleteTodoListRequest(NamedTuple):
    id: UUID


class DeleteTodoListResponse(NamedTuple):
    success: bool
    message: str


class DeleteTodoListUseCase(InputPort):

    def __call__(self, request: DeleteTodoListRequest) -> DeleteTodoListResponse:
        response = None
        try:
            self._repository.delete(request.id)
            response = DeleteTodoListResponse(success=True, message='Todo List deleted.')
        except TodoListNotFoundError:
            response = DeleteTodoListResponse(success=False, message='Todo List not found.')
        return self._presenter(response)
