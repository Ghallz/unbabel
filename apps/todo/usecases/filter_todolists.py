from typing import NamedTuple, Dict, Tuple, List
from framework.usecases import InputPort, OutputPort, Adapter
from todo.domain import TodoListData


class FilterTodoListsRequest(NamedTuple):
    filters: Dict


class FilterTodoListsResponse(NamedTuple):
    success: bool
    message: str
    data: List

class FilterTodoListsUseCase(InputPort):

    def __call__(self, request: FilterTodoListsRequest) -> FilterTodoListsResponse:
        try:
            todolists = self._repository.filter(request.filters)
            response = FilterTodoListsResponse(
                success=True,
                message='',
                data=[todolist.__dict__ for todolist in todolists]
            )
        except Exception as e:
            response = FilterTodoListsResponse(
                success=False,
                message=str(e),
                data=()
            )
        return self._presenter(response)