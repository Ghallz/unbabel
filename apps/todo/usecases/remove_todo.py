from typing import NamedTuple
from framework.usecases.interfaces import InputPort, OutputPort, Adapter, Request, Response
from framework.infrastructure.interfaces import Repository
from todo.domain import TodoData, TodoListNotFoundError
from framework.domain import EntityId


class RemoveTodoRequest(Request):
    todolist_id: EntityId
    todo_id: EntityId


class RemoveTodoResponse(Response):
    success: bool
    message: str
    data: tuple


class RemoveTodoUseCase(InputPort):

    def __call__(self, request: RemoveTodoRequest) -> RemoveTodoResponse:
        try:
            todolist = self._repository.get(request.todolist_id)
            todolist.remove(request.todo_id)
            todolist = self._repository.save(todolist)
            response = RemoveTodoResponse(
                success=True,
                message='Todo removed',
                data=()
            )
        except TodoListNotFoundError:
            response = RemoveTodoResponse(
                success=False,
                message='Todo List Not Fount.',
                data=()
            )
        return self._presenter(response)
