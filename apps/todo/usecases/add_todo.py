from typing import NamedTuple, List
from todo.domain import TodoData
from framework.domain import EntityId
from framework.usecases.interfaces import InputPort, OutputPort, Adapter
from framework.infrastructure.interfaces import Repository
from todo.domain import Todo


class AddTodoRequest(NamedTuple):
    todolist_id: EntityId
    todo: TodoData


class AddTodoResponse(NamedTuple):
    success: bool
    data: TodoData


class AddTodoUseCase(InputPort):

    def __call__(self, request: AddTodoRequest) -> AddTodoResponse:
        todolist = self._repository.get(request.todolist_id)
        todolist.add(Todo(**request.todo._asdict()))
        todolist = self._repository.save(todolist)
        return self._presenter(
            AddTodoResponse(
                success=True, 
                data=TodoData(**todolist.todos[-1].__dict__)
            )
        )
