from typing import NamedTuple
from framework.usecases import InputPort, OutputPort, Adapter
from todo.domain import TodoListData, TodoListNotFoundError, EntityId

class GetTodoListRequest(NamedTuple):
    todolist_id: EntityId


class GetTodoListResponse(NamedTuple):
    success: bool
    message: str
    data: tuple


class GetTodoListUseCase(InputPort):

    def __call__(self, request: GetTodoListRequest) -> GetTodoListResponse:
        try:
            todolist = self._repository.get(request.todolist_id)
            response = GetTodoListResponse(
                success=True,
                message='',
                data=TodoListData(**todolist.__dict__)
            )
        except TodoListNotFoundError:
            response = GetTodoListResponse(
                success=False,
                message='Todo List Not Fount.',
                data=()
            )
        return self._presenter(response)