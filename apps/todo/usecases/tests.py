import json
from unittest import TestCase
from unittest.mock import patch

from framework.usecases import OutputPort
from todo.domain import Todo, TodoData, TodoList, TodoListData, TodoListNotFoundError
from todo.infrastructure import TodoListMockedRepository, TodoListRepository
from todo.usecases import (AddTodoRequest, AddTodoResponse,
                           AddTodoUseCase, CreateTodoListMockedAdapter,
                           CreateTodoListRequest,
                           CreateTodoListResponse, CreateTodoListUseCase,
                           DeleteTodoListRequest,
                           DeleteTodoListResponse, DeleteTodoListUseCase,
                           RemoveTodoRequest,
                           RemoveTodoResponse, RemoveTodoUseCase,
                           MoveTodoRequest, MoveTodoResponse,
                           MoveTodoUseCase, GetTodoListRequest, GetTodoListResponse,
                           GetTodoListUseCase,
                           FilterTodoListsRequest, FilterTodoListsResponse,
                           FilterTodoListsUseCase)


class UseCaseTestCase(TestCase):
    
    @patch(__name__ + '.OutputPort', autospec=True)
    @patch(__name__ + '.TodoListRepository', autospec=True)
    def test_create_todolist_usecase_mock(self, Repository, Presenter):
        data = {
            'id': 'd0f19d156e154946b74a8ed150d26e5d',
            'name': 'My Awesome Todo List',
            'todos': []
        }
        # Setup Mocked Repository response
        # Repository arguments don't need to be real things since it's mocked
        repository = Repository('ConcreteRepository', 'session')
        repository.save.return_value = TodoList(**data)
        # A real adapter is not necessary since the presenter is mocked
        presenter = Presenter(adapter='Bla')
        ## Actual call of the UseCase a.k.a business stuff
        usecase = CreateTodoListUseCase(
            repository=repository,
            presenter=presenter
        )
        request = CreateTodoListRequest(name='My Awesome Todo List')
        response = usecase(request)
        repository.save.assert_called_once_with(TodoList(id=None, name='My Awesome Todo List'))
        presenter.assert_called_once_with(CreateTodoListResponse(success=True, message='Todo List Created', data=TodoListData(**data)))

    @patch(__name__ + '.OutputPort', autospec=True)
    @patch(__name__ + '.TodoListRepository', autospec=True)
    def test_delete_todolist_usecase(self, Repository, Presenter):
        
        # Setup Mocks
        # The arguments do not need to be valid objects since repository 
        # and presenter are mocks
        repository = Repository('ConcreteRepository', 'session')

        presenter = Presenter('Adapter')

        # Setup the usecase using the mocks
        usecase = DeleteTodoListUseCase(
            repository=repository,
            presenter=presenter
        )

        # Use an arbitrary id just to simulate the request.
        request = DeleteTodoListRequest('d0f19d156e154946b74a8ed150d26e5d')
        
        # Simulate a succeeded delete response from repository.
        repository.delete.return_value = None

        # Run the usecase with the current mock repository and presenter.
        usecase(request)

        # Assert expected behavior
        repository.delete.assert_called_once_with('d0f19d156e154946b74a8ed150d26e5d')
        presenter.assert_called_once_with(DeleteTodoListResponse(success=True, message='Todo List deleted.'))
    
        # Simulate a failed delete response from repository.
        repository.delete.side_effect = TodoListNotFoundError

        usecase(request)

        # Assert expected behavior
        repository.delete.assert_called_with('d0f19d156e154946b74a8ed150d26e5d')
        presenter.assert_called_with(DeleteTodoListResponse(success=False, message='Todo List not found.'))

    @patch(__name__ + '.OutputPort', autospec=True)
    @patch(__name__ + '.TodoListRepository', autospec=True)
    def test_get_todolist(self, Repository, Presenter):
        repository = Repository('ConcreteRepository', 'session')
        presenter = Presenter('Adapter')

        repository.get.return_value = TodoList(
            id='d0f19d156e154946b74a8ed150d26e5d',
            name='My Todo List',
            todos=[]
        )

        usecase = GetTodoListUseCase(
            repository=repository,
            presenter=presenter
        )

        request = GetTodoListRequest(
            todolist_id='d0f19d156e154946b74a8ed150d26e5d'
        )

        usecase(request)

        repository.get.assert_called_once_with('d0f19d156e154946b74a8ed150d26e5d')

        presenter.assert_called_once_with(
            GetTodoListResponse(
                success=True,
                message='',
                data=TodoListData(
                    id='d0f19d156e154946b74a8ed150d26e5d',
                    name='My Todo List',
                    todos=[]
                )
            )
        )

    @patch(__name__ + '.OutputPort', autospec=True)
    @patch(__name__ + '.TodoListRepository', autospec=True)
    def test_filter_todolists(self, Repository, Presenter):
        repository = Repository('ConcreteRepository', 'session')
        presenter = Presenter('Adapter')

        repository.filter.return_value = [
            TodoList("d0f19d156e154946b74a8ed150d26e5d", "1st Todo List")
        ]

        usecase = FilterTodoListsUseCase(
            repository=repository,
            presenter=presenter
        )

        request = FilterTodoListsRequest(
            filters={
                'name': '1st Todo List'
            }
        )

        response = usecase(request)

        repository.filter.assert_called_once_with({'name': '1st Todo List'})

        presenter.assert_called_once_with(
            FilterTodoListsResponse(
                success=True,
                message='',
                data=[
                    TodoList("d0f19d156e154946b74a8ed150d26e5d", "1st Todo List").__dict__
                ]
            )
        )


    @patch(__name__ + '.OutputPort', autospec=True)
    @patch(__name__ + '.TodoListRepository', autospec=True)
    def test_add_todo_to_todolist(self, Repository, Presenter):
        # Setup Mocks
        repository = Repository('ConcreteRepository', 'session')
        presenter = Presenter('Adapter')
        # Initializes the usecase using the mocks
        usecase = AddTodoUseCase(
            repository=repository,
            presenter=presenter
        )
        # Example request to be used
        request = AddTodoRequest(
            todolist_id='d0f19d156e154946b74a8ed150d26e5d',
            todo=TodoData(
                id=None,
                description="My new todo",
                completed=False
            )
        )
        # TodoList to be used as mockup value
        test_todolist = TodoList(
            id='d0f19d156e154946b74a8ed150d26e5d',
            name='My Todo List')
        # Simulate the responses from repository
        repository.get.return_value = test_todolist
        repository.save.return_value = TodoList(
            id='d0f19d156e154946b74a8ed150d26e5d',
            name='My Todo List',
            todos=[
                Todo(id='68119ee1d21f40e986e3ea586fa0b68f', description='My new todo')
            ]
        )
        # The actual call of the usecase
        usecase(request)
        # Just add a todo to our test TodoList for mockup
        test_todolist.add(Todo(id=None, description="My new todo"))
        # Validate if repository was called with the right argument by the usecase
        repository.save.assert_called_once_with(test_todolist)
        # Validate if the presenter was called with the expected response object
        expected_response = AddTodoResponse(
            success=True, 
            data=TodoData(
                id='68119ee1d21f40e986e3ea586fa0b68f',
                description="My new todo",
                completed=False
            )
        )
        presenter.assert_called_once_with(expected_response)

    @patch(__name__ + '.OutputPort', autospec=True)
    @patch(__name__ + '.TodoListRepository', autospec=True)
    def test_remove_todo_from_todolist(self, Repository, Presenter):
        repository = Repository('ConcreteRepositroy', 'session')
        presenter = Presenter('Adapter')

        repository.get.return_value = TodoList(
            id='d0f19d156e154946b74a8ed150d26e5d',
            name='My Todo List',
            todos=[
                Todo(id='68119ee1d21f40e986e3ea586fa0b68f', description='My new todo')
            ]
        )

        repository.save.return_value = TodoList(
            id='d0f19d156e154946b74a8ed150d26e5d',
            name='My Todo List',
            todos=[]
        )

        usecase = RemoveTodoUseCase(
            repository=repository,
            presenter=presenter
        )

        request = RemoveTodoRequest(
            todolist_id='d0f19d156e154946b74a8ed150d26e5d',
            todo_id='68119ee1d21f40e986e3ea586fa0b68f'
        )

        usecase(request)

        repository.get.assert_called_once_with('d0f19d156e154946b74a8ed150d26e5d')
        repository.save.assert_called_once_with(
            TodoList(
                id='d0f19d156e154946b74a8ed150d26e5d',
                name='My Todo List',
                todos=[]
            )
        )

        presenter.assert_called_once_with(
            RemoveTodoResponse(
                success=True,
                message='Todo removed',
                data=()
            )
        )

    @patch(__name__ + '.OutputPort', autospec=True)
    @patch(__name__ + '.TodoListRepository', autospec=True)
    def test_move_todo_to_other_position(self, Repository, Presenter):
        repository = Repository('ConcreteRepositroy', 'session')
        presenter = Presenter('Adapter')

        repository.get.return_value = TodoList(
            id='d0f19d156e154946b74a8ed150d26e5d',
            name='My Todo List',
            todos=[
                Todo(id='68119ee1d21f40e986e3ea586fa0b68f', description='My new todo'),
                Todo(id='a613fdaa2aeb4551bb596a2870a912b6', description='My other todo')
            ]
        )

        usecase = MoveTodoUseCase(
            repository=repository,
            presenter=presenter
        )

        request = MoveTodoRequest(
            todolist_id='d0f19d156e154946b74a8ed150d26e5d',
            todo_id='a613fdaa2aeb4551bb596a2870a912b6',
            position=0
        )

        usecase(request)

        repository.get.assert_called_once_with('d0f19d156e154946b74a8ed150d26e5d')

        repository.save.assert_called_once_with(
            TodoList(
                id='d0f19d156e154946b74a8ed150d26e5d',
                name='My Todo List',
                todos=[
                    Todo(id='a613fdaa2aeb4551bb596a2870a912b6', description='My other todo'),
                    Todo(id='68119ee1d21f40e986e3ea586fa0b68f', description='My new todo')
                ]
            )
        )

        presenter.assert_called_once_with(
            MoveTodoResponse(
                success=True,
                message='',
                data=()
            )
        )