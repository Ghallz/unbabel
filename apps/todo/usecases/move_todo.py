from typing import NamedTuple
from framework.usecases import InputPort, OutputPort, Adapter, Response, Request
from framework.domain import EntityId


class MoveTodoRequest(Request):
    todolist_id: EntityId
    todo_id: EntityId
    position: int


class MoveTodoResponse(Response):
    success: bool
    message: str
    data: tuple


class MoveTodoUseCase(InputPort):

    def __call__(self, request: MoveTodoRequest) -> MoveTodoResponse:
        try:
            todolist = self._repository.get(request.todolist_id)
            todo = todolist[request.todo_id]
            todolist.move_to(request.position, todo)
            self._repository.save(todolist)
            response = MoveTodoResponse(success=True, message='', data=())
        except TodoListNotFoundError:
            response = MoveTodoResponse(
                success=False,
                message='Todo List Not Fount.',
                data=()
            )
        return self._presenter(response)