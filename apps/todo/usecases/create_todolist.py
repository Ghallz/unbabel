import json
from uuid import UUID
from typing import NamedTuple, List
from framework.usecases.interfaces import InputPort, OutputPort, Adapter
from framework.infrastructure.interfaces import Repository
from todo.domain import TodoList, TodoListData


#### Create Todo List ####

class CreateTodoListRequest(NamedTuple):
    name: str


class CreateTodoListResponse(NamedTuple):
    success: bool
    message: str
    data: TodoListData


class CreateTodoListUseCase(InputPort):

    def __call__(self, request: CreateTodoListRequest) -> CreateTodoListResponse:
        todolist = self._repository.save(TodoList(id=None, name=request.name))
        return self._presenter(
            CreateTodoListResponse(
                success=True,
                message='Todo List Created',
                data=TodoListData(**todolist.__dict__)
            )
        )

class CreateTodoListMockedAdapter(Adapter):
    
    def __call__(self, data: CreateTodoListResponse):
        return json.dumps(data._asdict())