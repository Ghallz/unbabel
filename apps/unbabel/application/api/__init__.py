from flask import Blueprint
from flask_restful import Api

from .resources import HackerNews


hacker_news_api_blueprint = Blueprint('hacker-news', __name__)
api = Api(hacker_news_api_blueprint)

api.add_resource(HackerNews, '/hacker-news/')
