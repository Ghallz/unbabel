from flask_restful import Resource
from ...usecases import GetStoriesUseCase
from ...application import presenters
from .adapters import HackerNewsApiAdapter
from ...infrastructure import HackerNewsService


class HackerNews(Resource):

    def get(self, id=None):

        usecase = GetStoriesUseCase(
            service=HackerNewsService(),
            presenter=presenters.GetStoriesPresenter(adapter=HackerNewsApiAdapter())
        )

        return usecase()