from marshmallow import Schema, fields


class CommentSchema(Schema):
    comment_id = fields.Int()
    author = fields.Str()
    text = fields.Str()
    date_created = fields.Int()


class StorySchema(Schema):
    story_id = fields.Int()
    author = fields.Str()
    title = fields.Str()
    url = fields.Str()
    date_created = fields.Int()
    comments = fields.Nested(CommentSchema, many=True)


class ErrorResponseSchema(Schema):
    error = fields.Str()