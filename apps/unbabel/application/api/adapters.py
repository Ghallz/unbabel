from .serializers import StorySchema, ErrorResponseSchema


class HackerNewsApiAdapter:

    def __call__(self, response):
        if response.success:
            schema = StorySchema()
            if isinstance(response.data, list):
                return schema.dump(response.data, many=True), 200
            else:
                return schema.dump(response.data), 200
        else:
            schema = ErrorResponseSchema()
            return schema.dump({'error': response.message}), 404