import requests

from ...domain import Story, Comment


class HackerNewsService:

    def get_stories(self):
        json_data = requests.get(' https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty').json()
        stories = []
        for story_id in json_data[:2]:
            story = requests.get('https://hacker-news.firebaseio.com/v0/item/'+str(story_id)+'.json?print=pretty').json()
            story_return = self.story_to_entity(story)
            if story['kids']:
                for comment_id in story['kids'][:3]:
                        comment = requests.get('https://hacker-news.firebaseio.com/v0/item/'+str(comment_id)+'.json?print=pretty').json()
                        story_return.comments.append(self.comment_to_entity(comment))
                stories.append(story_return)
        return stories

    def story_to_entity(self, story):
        story_entity = Story()
        story_entity.story_id = story['id']
        story_entity.author = story['by']
        story_entity.title = story['title']
        story_entity.url = story['url']
        story_entity.date_created = story['time']
        return story_entity

    def comment_to_entity(self, comment):
        comment_entity = Comment()
        comment_entity.comment_id= comment['id']
        comment_entity.author= comment['by']
        comment_entity.text= comment['text']
        comment_entity.date_created= comment['time']
        return comment_entity


