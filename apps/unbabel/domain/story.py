class Story:
    def __init__(self, story_id: str = None, author: str = None, title: str = None, url: str = None, date_created: str = None, comments: list= []) -> None:
        self.story_id = story_id
        self.author = author
        self.title = title
        self.url = url
        self.date_created = date_created
        self.comments = comments
