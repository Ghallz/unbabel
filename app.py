from datetime import timedelta

from flask import Flask, request

from extensions import db


def create_app():
    app = Flask(__name__)

    configure_jwt(app)
    configure_db()

    from apps.hacker_news.application.api import blueprints
    configure_blueprints(app, blueprints)
    return app


def configure_jwt(app):
    app.config['JWT_SECRET_KEY'] = 'B9KzOhrFY88XKSaXirSQ6fNIIRfH5I0u'
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(30)
    app.config['JWT_REFRESH_TOKEN_EXPIRES'] = timedelta(60)
    jwt = JWTManager(app)


def configure_db():
    from .feeds.infrastructure.database import FeedStore
    from .users.infrastructure.database import UserStore

    try:
        db.bind(provider='sqlite', filename='database.sqlite', create_db=True)
    except TypeError:
        pass
    else:
        db.generate_mapping(check_tables=False)

    db.create_tables()


def configure_blueprints(app, blueprints):
    """Configure blueprints in views"""
    for blueprint in blueprints:
        if isinstance(blueprint, str):
            blueprint = getattr(blueprints, blueprint)
        app.register_blueprint(blueprint)


app = create_app()

if __name__ == '__main__':
    app.run()
