FROM python:latest

ENV PYTHONUNBUFFERED 1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install -U pip && pip install -Ur requirements.txt

ADD . /usr/src/app
WORKDIR /usr/src/app
